
# Package AIR 64-bit and 32-Bit android sample

  

Sample VSCode project with setup for packaging AIR for Android.


# Prerequisites

  

VSCode with ActionScript & MXML extension https://github.com/BowlerHatLLC/vscode-as3mxml

  

Harman AIR 33 (v33.1.1.345 as of today) set up in VSCode (and make sure ADT is in you environment variables)

https://airsdk.harman.com/download

  

Create a certificate named MyCert.p12 with password 1234 (this is just for testing purposes) and place it inside the cert folder.

https://help.adobe.com/en_US/air/build/WS901d38e593cd1bac1e63e3d128fc240122-7ffc.html

`adt -certificate -validityPeriod 25 -cn MyCert 2048-RSA "cert/MyCert.p12" 1234`

# Generating APK's

Build the project with the [debug or release build task](https://github.com/BowlerHatLLC/vscode-as3mxml/wiki/Build-an-ActionScript-project-in-Visual-Studio-Code "debug or release build task") , then run:

## 64-bit apk

`adt -package -target apk-captive-runtime -arch armv8 -storetype pkcs12 -keystore cert/MyCert.p12 -storepass 1234 bin/Main-64bit.apk bin/Main-app.xml -C bin Main.swf icons/.`


## 32-bit apk

The 32-bit apk can be generated with the VSCode extension task as well.

`adt -package -target apk-captive-runtime -storetype pkcs12 -keystore cert/MyCert.p12 -storepass 1234 bin/Main-32bit.apk bin/application.xml -C bin Cantidades.swf icons/.`


# Notes 

src/Main.app.xml is set up to targetSdkVersion="29" per [Google's guidelines](https://developer.android.com/distribute/best-practices/develop/target-sdk?hl=en "Google's guidelines") 

When publishing to the Playstore 32-bit and 64-bit must have different versionNumber in the Main-app.xml, so remember to change it.
